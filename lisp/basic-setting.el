(defun open-init-file ()
  "find init file , open it in a new buffer and display it in front of the user."
  (interactive)
  (find-file "~/.emacs.d/init.el"))

(global-set-key (kbd "<f2>") 'open-init-file)

(global-set-key "\C-x\C-b" 'buffer-menu)

(defun line-to-top-of-window ()
  "Move the line point is on to top of window."
  (interactive)
  (recenter 0))
(global-set-key "\C-c\C-c1" 'line-to-top-of-window)
(global-set-key "\C-c\C-cdc" 'list-colors-display)
(global-set-key "\C-c\C-cmb" 'menu-bar-mode)

(global-set-key "\C-c\C-csb" 'scroll-bar-mode)
(global-set-key "\C-c\C-cdlm" 'display-line-numbers-mode)
(global-set-key "\C-c\C-cdf" 'list-faces-display)

(global-set-key "\C-cfs"
		'toggle-frame-maximized)

(setq auto-save-default nil)

(icomplete-mode)

(setq make-backup-files nil)

(setq bookmark-save-flag 1)

(setq display-raw-bytes-as-hex t)

(setq w32-apps-modifier 'super)

(provide 'basic-setting)
