;; Configure network proxy
(setq my-proxy "127.0.0.1:7890")

(defun show-proxy ()
  "Show http/https proxy."
  (interactive)
  (if (boundp 'url-proxy-services)
      (message "Current proxy is \"%s\"" my-proxy)
    (message "No proxy")))


(defun set-proxy ()
  "Set http/https proxy."
  (interactive)
  (setq url-proxy-services `(("http" . ,my-proxy)
                             ("https" . ,my-proxy)))
  (show-proxy))

(defun unset-proxy ()
  "Unset http/https proxy."
  (interactive)
  (setq url-proxy-services nil)
  (show-proxy))

(defun toggle-proxy ()
  "Toggle http/https proxy."
  (interactive)
  (if (boundp 'url-proxy-services)
      (unset-proxy)
    (set-proxy)))

(toggle-proxy)

(provide 'proxy-setting)
