(defun add-a-main ()
  "add a main func in the point."
  (interactive)
    (insert "int main(int argc,char *argv[]){")
    (move-end-of-line nil)
    (newline 2)
    (save-excursion
      (newline  2)
      (insert "    return 0;")
      (newline)
      (insert "}")
      (indent-region (point-min) (point-max))))

(define-key c-mode-map "\C-cmain"
  'add-a-main)

(defun add-a-func (func-return-type func-name)
  "add a c func interactively according the arrguments(func-return-type,func-name)provided."
  (interactive "sinput the return type of the func: \nsinput the func name: ")
  (insert func-return-type " " func-name "(){")
    (move-end-of-line nil)
    (newline 2)
    (save-excursion
      (newline 2)
      (insert "}")
      (indent-region (point-min) (point-max))))

(define-key c-mode-map "\C-cfunc"
  'add-a-func)
