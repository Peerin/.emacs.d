(setq display-line-numbers-type 't)

(global-display-line-numbers-mode)

(tool-bar-mode -1)

(menu-bar-mode -1)

(scroll-bar-mode -1)

(toggle-frame-maximized)

(set-face-attribute 'default nil :height 140)

(global-hl-line-mode 1)

(set-face-background 'hl-line "light sky blue")

(set-face-background 'line-number-current-line "light sky blue")


;;;customize my own theme!!
(defun install-mytheme ()
  (let
      ((frame-background-color "darkseagreen2"))
    (if (not (string= frame-background-color (frame-parameter nil 'background-color)))
	(set-background-color frame-background-color))))
(defun make-frame-command-new ()
  "new one of make-frame-command-new for appling my own theme at all frame."
  (interactive)
  (select-frame (make-frame))
  (install-mytheme))
(global-set-key "\C-x52" 'make-frame-command-new)
(install-mytheme)

(provide 'theme-setting)
