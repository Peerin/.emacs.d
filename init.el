(add-to-list 'load-path "~/.emacs.d/lisp")

(require 'basic-setting)

(require 'proxy-setting)

(require 'theme-setting)

(setq c-mode-hook '(lambda ()
		     (load "c-mode-enhance")))

(require 'wy-auto-save)
;;启用必须在自定义变量设置之后，否则会失效，所以 enable 函数在customize-set-variables之后

(require 'posframe)

(require 'go-translate)
(setq gts-translate-list '(("en" "zh")))
(setq gts-default-translator
      (gts-translator
       :picker (gts-prompt-picker)
       :engines (list (gts-bing-engine) (gts-google-engine))
       :render (gts-buffer-render)))
(global-set-key (kbd "C-c t") 'gts-do-translate)

(require 'eglot)
(add-to-list 'eglot-server-programs '((c++-mode c-mode) "C:\\Users\\Peerin\\scoop\\apps\\clangd\\15.0.3\\bin\\clangd.exe"))
(add-hook 'c-mode-hook 'eglot-ensure)
(add-hook 'c++-mode-hook 'eglot-ensure)

(require 'elisp-demos)
(advice-add 'describe-function-1 :after #'elisp-demos-advice-describe-function-1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default-input-method "chinese-py")
 '(wy-auto-save-idle 1)
 '(wy-auto-save-slient t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(wy-auto-save-enable)
